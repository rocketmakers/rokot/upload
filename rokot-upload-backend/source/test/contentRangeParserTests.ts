import { expect } from "rokot-test";

import { ContentRangeParser } from "../core/contentRangeParser";

describe("ContentRangeParser", () => {
  let sut: ContentRangeParser;

  beforeEach(() => {
    sut = new ContentRangeParser();
  });

  describe("#parseContentRange", () => {
    it("returns object with correct startAt", () => {
      const result = sut.parseContentRange("bytes 500-999/1234");
      expect(result.startAt).to.equal(500);
    });

    it("returns object with correct endAt", () => {
      const result = sut.parseContentRange("bytes 500-999/1234");
      expect(result.endAt).to.equal(999);
    });

    it("returns object with correct byteCount", () => {
      const result = sut.parseContentRange("bytes 500-999/1234");
      expect(result.byteCount).to.equal(500);
    });

    it("returns object with correct totalLength", () => {
      const result = sut.parseContentRange("bytes 500-999/1234");
      expect(result.totalLength).to.equal(1234);
    });

    it("returns object with isLastChunk false when last byte not included", () => {
      const result = sut.parseContentRange("bytes 500-999/1234");
      expect(result.isLastChunk).to.be.false;
    });

    it("returns object with isLastChunk true when last byte included", () => {
      const result = sut.parseContentRange("bytes 1000-1233/1234");
      expect(result.isLastChunk).to.be.true;
    });

    it("throws when no input is given", () => {
      expect(() => sut.parseContentRange(null)).to.throw();
    });

    it("throws when format is incorrect", () => {
      expect(() => sut.parseContentRange("500, 999 / 1234")).to.throw();
    });
  });
});
