import * as stream from "stream";
import { IFileStore, IUploadMetadata, IContentRange, IFinalizeResult, MetadataMap } from "../../core/core";

class FileStoreTester implements IFileStore {
  constructor(private fileStore: IFileStore) {
  }

  fileCopy(fromKey: string, toKey: string): Promise<boolean> {
    throw new Error("Method not implemented.");
  }
  fileExists(key: string): Promise<boolean> {
    throw new Error("Method not implemented.");
  }
  readFile(key: string): Promise<stream.Readable> {
    throw new Error("Method not implemented.");
  }
  upload<TAdditionalMetadata extends MetadataMap>(body: string | stream.Readable | Buffer, filename: string, metadata: IUploadMetadata<TAdditionalMetadata>): Promise<string> {
    return this.fileStore.upload(body, filename, metadata)
  }
  getFileAdditionalMetadata<TAdditionalMetadata extends MetadataMap>(filename: string): Promise<TAdditionalMetadata> {
    throw new Error("Method not implemented.");
  }
  createFile<TAdditionalMetadata extends MetadataMap>(filename: string, metadata: IUploadMetadata<TAdditionalMetadata>): Promise<string> {
    throw new Error("Method not implemented.");
  }
  writeChunk(ticket: string, data: Buffer, partNumber: number, contentRange: IContentRange): Promise<void> {
    throw new Error("Method not implemented.");
  }
  finalize<TAdditionalMetadata extends MetadataMap>(ticket: string): Promise<IFinalizeResult<TAdditionalMetadata>> {
    throw new Error("Method not implemented.");
  }
}

describe("FileStore", () => {
  describe("#createFile", () => {
    it("calls createFile on file store", async () => {
      const metadata = { name: "me.jpg", size: 256, contentType: "image/jpeg", additionalMetadata: { userId: 1 } };
    });
  });
});
