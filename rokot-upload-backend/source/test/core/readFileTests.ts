import { expect } from "rokot-test";
import { AzureFileStore } from "../../core/storage/azureFileStore"

function getEnvKey(key: string) {
  return process.env[key] as string
}
describe("Azure - Read File", function () {
  this.timeout(50000);
  describe("#createFile", () => {
    it("calls createFile on file store", async () => {
      const connectionString = getEnvKey("AZURE_CONNECTION_STRING")
      if (!connectionString) {
        console.log("SKIPPED - missing env variable 'AZURE_CONNECTION_STRING'")
        return;
      }

      const store = new AzureFileStore({ containerName: "public", downloadChunkSize: 1024 * 1024 * 5, azure: { connectionString } })

      const readable = await store.readFile("5690a3df-60dc-47ca-b4d8-7a1fce69ce13")
      expect(readable).to.not.undefined
      expect(readable["_streamEnded"]).to.false
    });
  });
});
