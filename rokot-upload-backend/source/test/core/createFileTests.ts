import { expect, sinon } from "rokot-test";

import { IFileStore, IFilenameGenerator } from "../../core/core";
import { CreateFileService } from "../../core/services/createFileService";

describe("CreateFileService", () => {
  let sut: CreateFileService;
  let createFileSpy: sinon.SinonSpy;

  beforeEach(() => {
    createFileSpy = sinon.spy();

    const fileStore: IFileStore = <any>{
      createFile: createFileSpy
    };

    const filenameGenerator: IFilenameGenerator = <any>{
      generateFilename: sinon.stub().returns("filename")
    };

    sut = new CreateFileService(fileStore, filenameGenerator);
  });

  describe("#createFile", () => {
    it("calls createFile on file store", async () => {
      const metadata = { name: "me.jpg", size: 256, contentType: "image/jpeg", additionalMetadata: { userId: "1" } };
      await sut.createFile<{ userId: string }>(metadata);
      expect(createFileSpy.calledWith("filename", metadata)).to.be.true;
    });
  });
});
