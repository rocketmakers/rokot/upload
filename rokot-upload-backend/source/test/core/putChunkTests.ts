import { expect, sinon } from "rokot-test";

import { IFileStore } from "../../core/core";
import { PutChunkService } from "../../core/services/putChunkService";
import { ContentRangeParser } from "../../core/contentRangeParser";

describe("PutChunkService", () => {
  let sut: PutChunkService;
  let writeChunkSpy: sinon.SinonSpy;

  beforeEach(() => {
    writeChunkSpy = sinon.spy();

    const fileStore: IFileStore = <any>{
      writeChunk: writeChunkSpy,
      finalize: sinon.stub().returns({ filename: "a.txt", additionalMetadata: { type: "txt" } })
    };

    sut = new PutChunkService(fileStore, new ContentRangeParser());
  });

  describe("#putChunk", () => {
    it("calls putChunk on file store", async () => {
      const buffer = Buffer.alloc(100);
      await sut.putChunk("ticket", buffer, 1, "bytes 0-99/100");
      expect(writeChunkSpy.calledWith("ticket", buffer, 1)).to.be.true;
    });
  });
});
