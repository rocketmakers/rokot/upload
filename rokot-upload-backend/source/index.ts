export { CreateFileService } from "./core/services/createFileService";
export { PutChunkService } from "./core/services/putChunkService";

export { UuidFilenameGenerator } from "./core/storage/uuidFilenameGenerator";
export { AzureFileStore, IAzureConfig, IAzureFileStoreConfig, isAzureFileStoreConfig } from "./core/storage/azureFileStore";
export { GCSFileStore, IGCSConfig, IGCSFileStoreConfig, isGCSFileStoreConfig } from "./core/storage/gcsFileStore";
export { S3FileStore, IS3FileStoreConfig, isS3FileStoreConfig, IAwsConfig } from "./core/storage/s3FileStore";
export { LocalFileStore, ILocalFileStoreConfig, isLocalFileStoreConfig } from "./core/storage/localFileStore";

export { ContentRangeParser } from "./core/contentRangeParser";
export { IUploadMetadata, IContentRange, IFileStore, IFileStoreConfig, IFilenameGenerator, IFileCreationResult } from "./core/core";
export { ICreateFileService, IPutChunkService} from "./core/services";

// export function makeCreateFileService(fileStore: IFileStore): CreateFileService {
//   return new CreateFileService(fileStore, new UuidFilenameGenerator());
// }
//
// export function makePutChunkService(fileStore: IFileStore): PutChunkService {
//   return new PutChunkService(fileStore, new ContentRangeParser());
// }
