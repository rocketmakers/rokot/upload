import * as stream from "stream";
import * as azure from "azure-storage";

import { IFileStore, IUploadMetadata, IContentRange, IFinalizeResult, MetadataMap, IFileStoreConfig } from "../core";
import { toMetadataMap } from './common';

export interface IAzureConfig {
  connectionString: string
}

export interface IAzureFileStoreConfig extends IFileStoreConfig {
  azure: IAzureConfig
  containerName: string
  downloadChunkSize?: number
}

stream.Duplex.prototype._read = function () {
  return this.read();
};
stream.Duplex.prototype._write = function (chunk, encoding, cb) {
  return this.write(chunk, encoding, cb);
};

export function isAzureFileStoreConfig(config: IFileStoreConfig): config is IAzureFileStoreConfig {
  return !!(config as IAzureFileStoreConfig).azure
}

export class AzureFileStore implements IFileStore {
  private blobService: azure.BlobService
  constructor(
    private config: IAzureFileStoreConfig
  ) {
    this.blobService = azure.createBlobService(config.azure.connectionString)
  }

  fileCopy(fromKey: string, toKey: string): Promise<boolean> {
    return new Promise<boolean>((res, rej) => {
      const uri = this.blobService.getUrl(this.config.containerName, fromKey)
      this.blobService.startCopyBlob(uri, this.config.containerName, toKey, (error, result, response) => {
        if (error) {
          return res(false)
        }

        res(result.exists)
      })
    })
  }

  fileExists(key: string): Promise<boolean> {
    return new Promise<boolean>((res, rej) => {
      this.blobService.doesBlobExist(this.config.containerName, key, (error, result, response) => {
        if (error) {
          return res(false)
        }

        res(result.exists)
      })
    })
  }

  /** Reads a file using the supplied key */
  async readFile(filename: string): Promise<stream.Readable> {
    return this.blobService.createReadStream(this.config.containerName, filename, (error, result, response) => {
      // if (error) {
      //   return rej(error)
      // }
      // res(stream)
    })
  }

  /** Uploads data directly into storage */
  upload<TAdditionalMetadata extends MetadataMap>(body: string | Buffer | stream.Readable, filename: string, metadata: IUploadMetadata<TAdditionalMetadata>): Promise<string> {
    return this.azureUploadFile(body, filename, toMetadataMap(metadata.additionalMetadata))
  }

  /** Gets a files additional metadata using the supplied key */
  async getFileAdditionalMetadata<TAdditionalMetadata extends MetadataMap>(filename: string): Promise<TAdditionalMetadata> {
    const metadata = await this.azureGetBlobMetadata(this.config.containerName, filename)
    return metadata.metadata as TAdditionalMetadata
  }

  async createFile<TAdditionalMetadata extends MetadataMap>(filename: string, metadata: IUploadMetadata<TAdditionalMetadata>): Promise<string> {
    const result = await this.azureCreateAppendBlob(this.config.containerName, filename, toMetadataMap(metadata.additionalMetadata));
    if (!result.isSuccessful) {
      throw new Error("Unable to create file for upload")
    }
    return filename;
  }

  async writeChunk(ticket: string, data: Buffer, partNumber: number, contentRange: IContentRange): Promise<void> {
    const bytesThisPart = contentRange.byteCount;

    await this.azureAppendBlockFromStream(this.config.containerName, ticket, data, bytesThisPart);
  }

  async finalize<TAdditionalMetadata extends MetadataMap>(ticket: string): Promise<IFinalizeResult<TAdditionalMetadata>> {
    const metadata = await this.azureGetBlobMetadata(this.config.containerName, ticket)
    return {
      filename: ticket,
      additionalMetadata: metadata.metadata as TAdditionalMetadata
    };
  }

  private azureUploadFile(body: string | Buffer | stream.Readable, filename: string, metadata: MetadataMap): Promise<string> {
    return new Promise<string>((res, rej) => {
      if (Buffer.isBuffer(body) || typeof body === "string") {
        this.blobService.createAppendBlobFromText(this.config.containerName, filename, body, { metadata }, function (error, result, response) {
          if (error) {
            return rej(error)
          }

          res(filename)
        });
        return
      }

      body.pipe(this.blobService.createWriteStreamToBlockBlob(this.config.containerName, filename, { metadata }, function (error, result, response) {
        if (error) {
          return rej(error)
        }

        res(filename)
      }));
    })
  }

  private azureCreateAppendBlob(container: string, blob: string, metadata: MetadataMap): Promise<azure.ServiceResponse> {
    return new Promise<azure.ServiceResponse>((res, rej) => this.blobService.createOrReplaceAppendBlob(container, blob, { metadata }, (err, data) => err ? rej(err) : res(data)));
  }

  private azureGetBlobMetadata(container: string, blob: string): Promise<azure.BlobService.BlobResult> {
    return new Promise<azure.BlobService.BlobResult>((res, rej) => this.blobService.getBlobMetadata(container, blob, (err, data) => err ? rej(err) : res(data)));
  }

  private azureSetBlobMetadata(container: string, blob: string, metadata: MetadataMap): Promise<azure.BlobService.BlobResult> {
    return new Promise<azure.BlobService.BlobResult>((res, rej) => this.blobService.setBlobMetadata(container, blob, metadata, (err, data) => err ? rej(err) : res(data)));
  }

  private azureAppendBlockFromStream(container: string, blob: string, data: Buffer, length: number): Promise<azure.BlobService.BlobResult> {
    const readable = new stream.Duplex();
    readable.push(data);
    return new Promise<azure.BlobService.BlobResult>((res, rej) => this.blobService.appendBlockFromStream(container, blob, readable, length, (err, data) => err ? rej(err) : res(data)));
  }
}