import { IFileStore, IFileStoreConfig, IUploadMetadata, IContentRange, IFinalizeResult, MetadataMap } from "../core";
import { AsyncFs } from "./fs/asyncFs";
import * as stream from 'stream';
import * as fs from "fs";

export interface ILocalFileStoreConfig extends IFileStoreConfig {
  pathProvider(ticket: string, extension: string): string
  createTicket?<TAdditionalMetadata extends MetadataMap>(filename: string, metadata: IUploadMetadata<TAdditionalMetadata>): string
}

export function isLocalFileStoreConfig(config: IFileStoreConfig): config is ILocalFileStoreConfig {
  return !!config["pathProvider"]
}

interface ILocalFileJson {
  filename: string
  metadata: { additionalMetadata: MetadataMap }
}

function createLocalFileJson(filename: string, metadata: any): ILocalFileJson {
  return { filename, metadata }
}

export class LocalFileStore implements IFileStore {
  constructor(private config: ILocalFileStoreConfig) {
  }

  fileCopy(fromKey: string, toKey: string): Promise<boolean> {
    const fromPath = this.config.pathProvider(fromKey, "upload")
    const toPath = this.config.pathProvider(toKey, "upload")
    return AsyncFs.copyFileAsync(fromPath, toPath)
  }

  fileExists(key: string): Promise<boolean> {
    const path = this.config.pathProvider(key, "upload")
    return AsyncFs.existsAsync(path)
  }

  async readFile(filename: string): Promise<stream.Readable> {
    const path = this.config.pathProvider(filename, "upload")
    return fs.createReadStream(path)
  }

  async getFileAdditionalMetadata<TAdditionalMetadata extends MetadataMap>(filename: string) {
    const path = this.config.pathProvider(filename, "json")
    const content = await AsyncFs.readFileAsync(path)
    const uploadMetadata = JSON.parse(content) as ILocalFileJson
    return uploadMetadata && uploadMetadata.metadata && uploadMetadata.metadata.additionalMetadata as TAdditionalMetadata
  }

  private createTicket<TAdditionalMetadata extends MetadataMap>(filename: string, metadata: IUploadMetadata<TAdditionalMetadata>) {
    return filename
  }

  async upload<TAdditionalMetadata extends MetadataMap>(body: string | Buffer | stream.Readable, filename: string, metadata: IUploadMetadata<TAdditionalMetadata>) {
    const ticket = this.config.createTicket ? this.config.createTicket(filename, metadata) : this.createTicket(filename, metadata);
    await AsyncFs.writeFileSafeAsync(this.config.pathProvider(ticket, "json"), JSON.stringify(createLocalFileJson(filename, metadata), null, 2))
    await AsyncFs.writeFileSafeAsync(this.config.pathProvider(ticket, "upload"), body)
    return ticket
  }

  async createFile<TAdditionalMetadata extends MetadataMap>(filename: string, metadata: IUploadMetadata<TAdditionalMetadata>): Promise<string> {
    const ticket = this.config.createTicket ? this.config.createTicket(filename, metadata) : this.createTicket(filename, metadata);
    await AsyncFs.writeFileSafeAsync(this.config.pathProvider(ticket, "json"), JSON.stringify(createLocalFileJson(filename, metadata), null, 2))
    return ticket
  }

  async writeChunk(ticket: string, data: Buffer, partNumber: number, contentRange: IContentRange): Promise<void> {
    const uploadFilePath = this.config.pathProvider(ticket, "upload");
    if (contentRange.startAt === 0) {
      return await this.writeFirstChunk(uploadFilePath, data, contentRange)
    }
    return await this.writeAppendChunk(uploadFilePath, data, contentRange)
  }

  private async writeFirstChunk(uploadFilePath: string, data: Buffer, contentRange: IContentRange): Promise<void> {
    const exists = await AsyncFs.existsAsync(uploadFilePath)
    if (exists) {
      throw new Error("Local file should not exist on first bytes")
    }

    await AsyncFs.writeFileSafeAsync(uploadFilePath, data);
  }

  private async writeAppendChunk(uploadFilePath: string, data: Buffer, contentRange: IContentRange): Promise<void> {
    const exists = await AsyncFs.existsAsync(uploadFilePath)
    if (!exists) {
      throw new Error("Local file should exist for append chunk")
    }

    const size = await AsyncFs.statSizeAsync(uploadFilePath);
    if (size !== contentRange.startAt) {
      throw new Error("Local file size doesnt meet content-range expectation")
    }

    await AsyncFs.appendFileAsync(uploadFilePath, data);
  }

  async finalize<TAdditionalMetadata extends MetadataMap>(ticket: string): Promise<IFinalizeResult<TAdditionalMetadata>> {
    var json = await AsyncFs.readFileAsync(this.config.pathProvider(ticket, "json"))
    var t: { filename: string, metadata: IUploadMetadata<TAdditionalMetadata> } = JSON.parse(json)
    return Promise.resolve<IFinalizeResult<TAdditionalMetadata>>({ filename: t.filename, additionalMetadata: t.metadata.additionalMetadata })
  }
}
