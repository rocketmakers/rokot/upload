import * as uuid from "node-uuid";
import { IFilenameGenerator } from "../core";

/** Generates UUID v4 id as an extensionless filename */
export class UuidFilenameGenerator implements IFilenameGenerator {
  /** Generate a UUID v4 id as an extensionless filename */
  generateFilename(): string {
    return uuid.v4();
  }
}
