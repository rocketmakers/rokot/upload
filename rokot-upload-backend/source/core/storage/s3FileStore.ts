import * as AWS from "aws-sdk";
import * as stream from 'stream';
import { IFileStore, IUploadMetadata, IContentRange, IFileStoreConfig, IFinalizeResult, MetadataMap } from '../core';
import { toMetadataMap } from './common';

export interface IAwsConfig {
  region: string
  accessKeyId: string
  secretAccessKey: string
}

export interface IS3FileStoreConfig extends IFileStoreConfig {
  aws: IAwsConfig
  bucketName: string
}

export function isS3FileStoreConfig(config: IFileStoreConfig): config is IS3FileStoreConfig {
  return !!(config as IS3FileStoreConfig).aws
}

export class S3FileStore implements IFileStore {
  private s3: AWS.S3
  constructor(private config: IS3FileStoreConfig) {
    AWS.config.update({ region: config.aws.region, credentials: new AWS.Credentials(config.aws.accessKeyId, config.aws.secretAccessKey) })
    this.s3 = new AWS.S3()
  }

  fileCopy(fromKey: string, toKey: string): Promise<boolean> {
    return this.s3.copyObject({
      Bucket: this.config.bucketName,
      Key: toKey,
      CopySource: `${this.config.bucketName}/${fromKey}`
    }).promise().then(r => {
      return true
    }).catch(err => {
      return false
    })
  }

  fileExists(key: string): Promise<boolean> {
    return this.s3.headObject({
      Bucket: this.config.bucketName,
      Key: key
    }).promise().then(r => {
      return !r.$response.error
    }).catch(err => {
      return false
    })
  }

  async readFile(filename: string): Promise<stream.Readable> {
    return this.s3.getObject({ Bucket: this.config.bucketName, Key: filename }).createReadStream()
  }

  async getFileAdditionalMetadata<TAdditionalMetadata extends MetadataMap>(filename: string): Promise<TAdditionalMetadata> {
    const head = await this.s3.headObject({
      Bucket: this.config.bucketName,
      Key: filename
    }).promise();

    return head.Metadata as TAdditionalMetadata
  }

  async upload<TAdditionalMetadata extends MetadataMap>(body: string | Buffer | stream.Readable, filename: string, metadata: IUploadMetadata<TAdditionalMetadata>): Promise<string> {
    const u = await this.s3.upload({
      Bucket: this.config.bucketName,
      Key: filename,
      ContentType: metadata.contentType,
      Metadata: toMetadataMap(metadata.additionalMetadata),
      Body: body
    }).promise()
    return u.Key
  }

  async createFile<TAdditionalMetadata extends MetadataMap>(filename: string, metadata: IUploadMetadata<TAdditionalMetadata>): Promise<string> {
    const multipart = await this.s3.createMultipartUpload({
      Bucket: this.config.bucketName,
      Key: filename,
      ContentType: metadata.contentType,
      Metadata: toMetadataMap(metadata.additionalMetadata)
    }).promise();

    return `${filename}|${multipart.UploadId}`
  }

  async writeChunk(ticket: string, data: Buffer, partNumber: number, contentRange: IContentRange): Promise<void> {
    const split = ticket.split("|");
    const key = split[0];
    const uploadId = split[1];
    const bytesThisPart = contentRange.byteCount;

    await this.s3.uploadPart({
      Bucket: this.config.bucketName,
      Key: key,
      UploadId: uploadId,
      PartNumber: partNumber,
      Body: data
    }).promise();
  }

  async finalize<TAdditionalMetadata extends MetadataMap>(ticket: string): Promise<IFinalizeResult<TAdditionalMetadata>> {
    const split = ticket.split("|");
    const key = split[0];
    const uploadId = split[1];
    const parts = await this.getAllParts(key, uploadId, [], 0);

    await this.s3.completeMultipartUpload({
      Bucket: this.config.bucketName,
      Key: key,
      UploadId: uploadId,
      MultipartUpload: {
        Parts: parts.map(part => ({ PartNumber: part.PartNumber, ETag: part.ETag }))
      }
    }).promise();

    const head = await this.s3.headObject({
      Bucket: this.config.bucketName,
      Key: key
    }).promise();

    return {
      filename: key,
      additionalMetadata: head.Metadata as TAdditionalMetadata
    };
  }

  private async getAllParts(key: string, uploadId: string, parts: any[], nextPartNumber: number): Promise<any[]> {
    const data = await this.s3.listParts({
      Bucket: this.config.bucketName,
      Key: key,
      UploadId: uploadId,
      PartNumberMarker: nextPartNumber
    }).promise();

    const newParts = parts.concat(...data.Parts)
    if (data.NextPartNumberMarker) {
      return await this.getAllParts(key, uploadId, newParts, data.NextPartNumberMarker);
    }

    return newParts;
  }
}
