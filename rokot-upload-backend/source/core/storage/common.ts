import { MetadataMap } from '../core';

export function toMetadataMap(additionalMetadata: any) : MetadataMap{
    if (additionalMetadata) {
      return Object.keys(additionalMetadata).reduce((memo, key) => {
        let value = additionalMetadata[key]
        if (value === null || value === undefined){
          return memo
        }
        if (typeof value === "object"){
          value = JSON.stringify(value)
        } else {
          value = value.toString()
        }
        return {...memo, [key]: value }
      }, {});
    }
    return {}
  }