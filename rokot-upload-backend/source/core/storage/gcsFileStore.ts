import * as stream from "stream";
import * as gcs from "@google-cloud/storage";
import axios from "axios";
import * as URL from "url";

import {
  IFileStore,
  IUploadMetadata,
  IContentRange,
  IFinalizeResult,
  MetadataMap,
  IFileStoreConfig
} from "../core";

export interface IGCSConfig {
  projectId: string;
}

export interface IGCSFileStoreConfig extends IFileStoreConfig {
  gcs: IGCSConfig;
  bucketName: string;
  downloadChunkSize?: number;
}

stream.Duplex.prototype._read = function () {
  return this.read();
};
stream.Duplex.prototype._write = function (chunk, encoding, cb) {
  return this.write(chunk, encoding, cb);
};

export function isGCSFileStoreConfig(
  config: IFileStoreConfig
): config is IGCSFileStoreConfig {
  return !!(config as IGCSFileStoreConfig).gcs;
}

export class GCSFileStore implements IFileStore {
  private bucketService: gcs.Bucket;
  constructor(config: IGCSFileStoreConfig) {
    const storage = new gcs.Storage();
    this.bucketService = storage.bucket(config.bucketName);
  }

  async fileCopy(fromKey: string, toKey: string): Promise<boolean> {
    //await this.bucketService.file(fromKey).copy(this.bucketService.file(toKey))
    throw "Not Implemented"
  }


  /** Checks if a file exists */
  async fileExists(key: string): Promise<boolean> {
    const file = this.bucketService.file(key);

    const [exists] = await file.exists();
    return exists;
  }

  /** Reads a file using the supplied key */
  async readFile(filename: string): Promise<stream.Readable> {
    const file: gcs.File = this.bucketService.file(filename);

    return file.createReadStream();
  }

  /** Uploads data directly into storage */
  async upload<TAdditionalMetadata extends MetadataMap>(
    body: string | Buffer | stream.Readable,
    filename: string,
    metadata: IUploadMetadata<TAdditionalMetadata>
  ): Promise<string> {
    const writeFileStream: stream.Writable = this.bucketService
      .file(filename)
      .createWriteStream({ metadata: { metadata: metadata.additionalMetadata }, resumable: false, contentType: metadata.contentType });

    return new Promise<string>((resolve, reject) => {
      if (Buffer.isBuffer(body) || typeof body === "string") {
        // Create our input stream
        const readable = new stream.Duplex();
        readable.push(body);

        readable.pipe(writeFileStream);
      } else {
        body.pipe(writeFileStream);
      }
      writeFileStream.on("finish", () => resolve());
      writeFileStream.on("error", () => reject());
    });
  }

  /** Gets a files additional metadata using the supplied key */
  async getFileAdditionalMetadata<TAdditionalMetadata extends MetadataMap>(filename: string): Promise<TAdditionalMetadata> {
    const [metadata] = await (this.bucketService.file(filename)).getMetadata();
    return metadata && metadata.metadata;
  }

  async createFile<TAdditionalMetadata extends MetadataMap>(
    filename: string,
    metadata: IUploadMetadata<TAdditionalMetadata>
  ): Promise<string> {
    const file = this.bucketService.file(filename);
    const response = await file.createResumableUpload({ metadata: { contentType: metadata.contentType, metadata: metadata.additionalMetadata, name: filename } });
    return response[0];
  }

  async writeChunk(
    ticket: string, // Our ticket is a uri from create File
    data: Buffer,
    partNumber: number,
    contentRange: IContentRange
  ): Promise<void> {
    const { startAt, endAt, byteCount, totalLength } = contentRange;
    try {
      await axios.put(ticket, data, {
        headers: {
          "Content-Length": byteCount,
          "Content-Range": `bytes ${startAt}-${endAt}/${totalLength}`
        }
      });
    }
    catch (err) {
      //If the request succeeds, the server responds with 308 Resume Incomplete,
      //along with a Range header that identifies the total number of bytes that have been stored so far
      if (!(err.response && err.response.status === 308 && err.response.headers["range"] === `bytes=0-${endAt}`)) {
        throw err
      }
    }
  }

  async finalize<TAdditionalMetadata extends MetadataMap>(ticket: string): Promise<IFinalizeResult<TAdditionalMetadata>> {
    const url = new URL.URL(ticket);
    const filename = url.searchParams.get("name");
    const metadata = await this.getFileAdditionalMetadata<TAdditionalMetadata>(filename);

    return {
      filename,
      additionalMetadata: metadata
    };
  }
}
