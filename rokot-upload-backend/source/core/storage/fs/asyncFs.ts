import * as fs from "fs";
import * as path from "path";
import * as stream from 'stream';

export class AsyncFs {
  static statSizeAsync(filename: string) {
    return new Promise<number>((res, rej) => {
      fs.stat(filename, (err, stat) => {
        if (err) {
          return rej(err);
        }
        res(stat.size)
      })
    })
  }

  static existsAsync(filePathOrFolder: string) {
    return new Promise<boolean>((res, rej) => {
      fs.exists(filePathOrFolder, exists => res(exists))
    })
  }

  static copyFileAsync(fromFilePath: string, toFilePath: string) {
    return new Promise<boolean>((res, rej) => {
      fs.copyFile(fromFilePath, toFilePath, err => res(!err))
    })
  }

  static readFileAsync(filePath: string) {
    return new Promise<string>((res, rej) => {
      fs.readFile(filePath, {}, (err, data) => {
        if (err) {
          return rej(err);
        }

        return res(data.toString());
      });
    })
  }

  static writeFileAsync(filePath: string, content: string | Buffer | stream.Readable) {
    return new Promise<void>((res, rej) => {

      if (typeof content === "string" || Buffer.isBuffer(content)) {
        fs.writeFile(filePath, content, err => {
          if (err) {
            return rej(err);
          }

          return res();
        });
        return
      }

      content.pipe(fs.createWriteStream(filePath)).on("close", res)
    })
  }

  static appendFileAsync(filePath: string, content: string | Buffer) {
    return new Promise<void>((res, rej) => {
      fs.appendFile(filePath, content, err => {
        if (err) {
          return rej(err);
        }

        return res();
      });
    })
  }
  static mkdirAsync(folder: string) {
    return new Promise<void>((res, rej) => {
      fs.mkdir(folder, err => {
        if (err) {
          rej(err)
          return
        }
        res()
      })
    })
  }

  static async mkdirRecursiveAsync(folder: string) {
    const exists = await this.existsAsync(folder)
    if (exists) {
      return;
    }
    await this.mkdirRecursiveAsync(path.dirname(folder))
    console.log(`-- Creating ${folder}`)
    await this.mkdirAsync(folder)
  }

  static mkfiledirRecursiveAsync(filename: string): Promise<void> {
    return this.mkdirRecursiveAsync(path.dirname(filename))
  }

  static async writeFileSafeAsync(filePath: string, content: string | Buffer | stream.Readable) {
    await this.mkfiledirRecursiveAsync(filePath)
    await this.writeFileAsync(filePath, content)
  }
}
