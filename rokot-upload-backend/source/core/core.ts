import * as stream from 'stream';

/**
 * Defines the metadata provided of the uploaded file
 */
export interface IUploadMetadata<TAdditionalMetadata extends MetadataMap> {
  /**
   * The name of the file
   */
  name: string;
  /**
   * The size of the file
   */
  size: number;
  /**
   * The content type
   */
  contentType: string;
  /**
   * Used to store application specific information about the upload
   */
  additionalMetadata: TAdditionalMetadata;
}

/**
 * Described a 'content-range' header
 */
export interface IContentRange {
  /** Starting at byte */
  startAt: number;
  /** Ending at byte */
  endAt: number;
  /** The number of byte */
  byteCount: number;
  /** The total length */
  totalLength: number;
  /** Is this the last chunk - used for finalization */
  isLastChunk: boolean;
}

/** Result of the finalized upload */
export interface IFinalizeResult<TAdditionalMetadata extends MetadataMap> {
  /** the uploaded file unique name */
  filename: string;
  /** the custom metadata attached to the upload from the client */
  additionalMetadata: TAdditionalMetadata;
}

export type MetadataMap = { [k: string]: string }

/**
 * Defines interactions of an upload file store
 */
export interface IFileStore {
  /** Copy a file to a different location within the container */
  fileCopy(fromKey: string, toKey: string): Promise<boolean>
  /** Indicates whether a file exists */
  fileExists(key: string): Promise<boolean>
  /** Reads a file using the supplied key */
  readFile(key: string): Promise<stream.Readable>
  /** Uploads data directly into storage */
  upload<TAdditionalMetadata extends MetadataMap>(body: string | Buffer | stream.Readable, filename: string, metadata: IUploadMetadata<TAdditionalMetadata>): Promise<string>
  /** Gets a files additional metadata using the supplied key */
  getFileAdditionalMetadata<TAdditionalMetadata extends MetadataMap>(filename: string): Promise<TAdditionalMetadata>
  /** Create a file ready for upload - this will return an upload ticket in the form `filename|storageToken|uri` */
  createFile<TAdditionalMetadata extends MetadataMap>(filename: string, metadata: IUploadMetadata<TAdditionalMetadata>): Promise<string>;
  /** Writes a file chunk using the ticket provided via 'createFile' */
  writeChunk(ticket: string, data: Buffer, partNumber: number, contentRange: IContentRange): Promise<void>;
  /** Finalize the upload when all chunks are written */
  finalize<TAdditionalMetadata extends MetadataMap>(ticket: string): Promise<IFinalizeResult<TAdditionalMetadata>>;
}

/** Indicator interface for File Store Config */
export interface IFileStoreConfig {
}

/** Provides filenames for uploads */
export interface IFilenameGenerator {
  /** Generates a unique filename for the upload */
  generateFilename(): string;
}

/** Result of File Creation Service */
export interface IFileCreationResult {
  /** the uploaded file unique name */
  filename: string;
  /** The upload ticket */
  ticket: string;
}

/** Upload Result Type (complete or in progress) */
export type IFileUploadResult<TAdditionalMetadata extends MetadataMap> = IFileUploadCompleteResult<TAdditionalMetadata> | IFileUploadInProgressResult;

/** Indicator that upload is complete */
export interface IFileUploadCompleteResult<TAdditionalMetadata extends MetadataMap> extends IFinalizeResult<TAdditionalMetadata> {
  /** Indicates the upload is complete */
  complete: true;
}

/** Indicator that upload is in progress */
export interface IFileUploadInProgressResult {
  /** Indicates the upload is in progress */
  complete: false;
}
