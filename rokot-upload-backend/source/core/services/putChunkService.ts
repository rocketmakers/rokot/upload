import { IFileStore, IFileUploadResult, MetadataMap } from "../core";
import { ContentRangeParser } from "../contentRangeParser";
import { IPutChunkService } from '../services';

export class PutChunkService implements IPutChunkService {
  constructor(
    private fileStore: IFileStore,
    private contentRangeParser: ContentRangeParser
  ) { }

  async putChunk<TAdditionalMetadata extends MetadataMap>(ticket: string, bytes: Buffer, part: number, contentRangeHeader: string): Promise<IFileUploadResult<TAdditionalMetadata>> {
    if (!contentRangeHeader) {
      throw new Error("No content range header");
    }

    if (isNaN(part)) {
      throw new Error("Bad part number");
    }

    const contentRange = this.contentRangeParser.parseContentRange(contentRangeHeader);
    if (contentRange.byteCount !== bytes.length) {
      throw new Error(`Expecting ${contentRange.byteCount}, got ${bytes.length}`);
    }

    await this.fileStore.writeChunk(ticket, bytes, part, contentRange);
    if (contentRange.isLastChunk) {
      const metadata = await this.fileStore.finalize<TAdditionalMetadata>(ticket);
      return {
        complete: true,
        filename: metadata.filename,
        additionalMetadata: metadata.additionalMetadata
      };
    }

    return { complete: false };
  }
}
