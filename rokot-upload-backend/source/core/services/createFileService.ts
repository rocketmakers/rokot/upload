import { IFileStore, IFilenameGenerator, IUploadMetadata, IFileCreationResult, MetadataMap } from "../core";
import { ICreateFileService } from '../services';

export class CreateFileService implements ICreateFileService {
  constructor(
    private fileStore: IFileStore,
    private filenameGenerator: IFilenameGenerator
  ) { }

  async createFile<TAdditionalMetadata extends MetadataMap>(metadata: IUploadMetadata<TAdditionalMetadata>, fileNamePrefix?: string): Promise<IFileCreationResult> {
    let filename = this.filenameGenerator.generateFilename();
    if (fileNamePrefix) {
      filename = `${fileNamePrefix}/${filename}`
    }
    const ticket = await this.fileStore.createFile(filename, metadata);
    return { filename, ticket };
  }
}
