import { IUploadMetadata, IFileCreationResult, IFileUploadResult, MetadataMap } from "./core";

/** Defines the File Creation Service, prepares the file store for upload */
export interface ICreateFileService {
  /** prepares the file store for upload and returns upload ticket */
  createFile<TAdditionalMetadata extends MetadataMap>(metadata: IUploadMetadata<TAdditionalMetadata>, fileNamePrefix?: string): Promise<IFileCreationResult>;
}

/** Defines the Put Chunk Service, uploads file chunks into the file store */
export interface IPutChunkService {
  /** Puts file chunks into the file store */
  putChunk<TAdditionalMetadata extends MetadataMap>(ticket: string, bytes: Buffer, part: number, contentRangeHeader: string): Promise<IFileUploadResult<TAdditionalMetadata>>;
}
