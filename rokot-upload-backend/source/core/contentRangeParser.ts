import { IContentRange } from "./core";

/** Parses http `content-range` header into a IContentRange  */
export class ContentRangeParser {
  /** Parses http `content-range` header into a IContentRange  */
  parseContentRange(contentRangeHeader: string): IContentRange {
    if (!contentRangeHeader) {
      throw new Error("no content range header");
    }

    const rx = /^bytes (\d+)-(\d+)\/(\d+)$/;
    const match = rx.exec(contentRangeHeader);
    if (!match) {
      throw new Error("does not match regex");
    }

    const startAt = parseInt(match[1]);
    const endAt = parseInt(match[2]);
    const totalLength = parseInt(match[3]);

    return {
      startAt,
      endAt,
      byteCount: endAt - startAt + 1,
      totalLength: totalLength,
      isLastChunk: endAt === totalLength - 1
    }
  }
}
