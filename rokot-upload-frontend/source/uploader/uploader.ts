import { IUploaderConfig, IBlobReader, IThumbnailProvider, IChunkSizeProvider } from "./config";
import { TaskQueueStatus, SequentialTaskQueueProcessor, isFinalStatus } from "./tasks/taskProcessor";
import { UploadTaskProvider, IFileUploadManager } from "./uploadTaskProvider";
import { XMLHttpRequestUploaderConfig } from "./chunks/xmlHttpRequestUploader";

export { TaskResultStatus } from "./tasks/task";
export { TaskQueueStatus, TaskProcessingResult } from "./tasks/taskProcessor";
export { IFileUploadManager } from "./uploadTaskProvider";

export class TaskQueueStatusHelper {
  static canRetry(status: TaskQueueStatus) {
    return status === TaskQueueStatus.Error;
  }
  static isFinal(status: TaskQueueStatus) {
    return isFinalStatus(status);
  }
}

/** Describes the client side uploader */
export interface IUploader {
  /** Returns the current set of uploads */
  getUploads(): IFileUploadManager[];
  /** Enqueue Files for upload, if you provide a single element array for additionalMetadata it will be used for all files */
  enqueueFiles<TMetadata>(files: File[], additionalMetadata: TMetadata[], customFinalizer?: (upload: IFileUploadManager) => Promise<void>): UploadTaskProvider[]
  /** subscribe to notification of changes */
  subscribe(onChange: () => void): () => void;
}

export function createXmlHttpUploader(url: string, withCredentials: boolean, blobReader: IBlobReader, thumbnailProvider: IThumbnailProvider, chunkSizeProvider: IChunkSizeProvider, headers?: () => { [name: string]: string }): IUploader {
  return createUploader(new XMLHttpRequestUploaderConfig(url, withCredentials, blobReader, thumbnailProvider, chunkSizeProvider, headers));
}

export function createUploader(config: IUploaderConfig): IUploader {
  return new Uploader(config);
}

class Uploader implements IUploader {
  getUploads() {
    return this.processer.queues.map(u => (<UploadTaskProvider>u).upload)
  }

  private processer: SequentialTaskQueueProcessor;
  constructor(private config: IUploaderConfig) {
    this.processer = new SequentialTaskQueueProcessor(() => this.emitChange());
  }

  enqueueFiles<TMetadata>(files: File[], additionalMetadata: TMetadata[], customFinalizer?: (upload: IFileUploadManager) => Promise<void>): UploadTaskProvider[] {
    return files.map((f, i) => {
      return this.createUploadManager(f, additionalMetadata[i] || additionalMetadata[0] || {}, customFinalizer)
    });
  }

  private createUploadManager(file: File, additionalMetadata: any, customFinalizer?: (upload: IFileUploadManager) => Promise<void>) {
    const taskProvider = new UploadTaskProvider(file, additionalMetadata, this.config, this.processer, () => this.emitChange(), customFinalizer);
    this.processer.enqueue(taskProvider);
    return taskProvider;
  }

  private hooks: (() => void)[] = [];
  subscribe(onChange: () => void): () => void {
    this.hooks.push(onChange);
    return () => this.hooks = this.hooks.filter(h => h !== onChange);
  }

  private emitChange() {
    this.hooks.forEach(h => h());
  }
}
