import * as React from "react";

import { IUploader, createXmlHttpUploader } from "./uploader";
import { IFileUploadManager } from "./uploadTaskProvider";
import { BlobReader } from "./blobReader";
import { ThumbnailProvider } from "./thumbnailProvider";
import { ChunkSizeProvider } from "./chunkSizeProvider";

export interface IFileUploadWrapperProps {
  multiple?: boolean;
  accept?: string;
  additionalMetadataProvider?: (files: File[]) => Promise<any[]>;
  onDone?: (upload: IFileUploadManager) => Promise<void>;
}

export interface IFileUploadComponentProps {
  uploads: IFileUploadManager[];
  openFileDialog: () => void;
  onDrop: (e: React.FormEvent<any>) => void;
}

export function withFileUpload<T>(url: string, withCredentials: boolean, headers?: () => { [name: string]: string }, chunkSize?: number) {
  function createUploader(): IUploader {
    return createXmlHttpUploader(url,
      withCredentials,
      new BlobReader(),
      new ThumbnailProvider(),
      new ChunkSizeProvider(chunkSize),
      headers);
  }

  return (Component: React.ComponentClass<T & IFileUploadComponentProps>): React.ComponentClass<T & IFileUploadWrapperProps> => class FileUploadWrapper extends React.Component<T & IFileUploadWrapperProps, {}> {
    private inputRef: HTMLInputElement;
    private uploader: IUploader;
    private unhook: () => void;

    componentWillMount() {
      this.uploader = createUploader();
      this.unhook = this.uploader.subscribe(() => this.forceUpdate());
    }

    componentWillUnmount() {
      this.unhook();
    }

    render() {
      return (
        <div>
          <Component {...this.props as any} uploads={this.uploader.getUploads()} openFileDialog={() => this.openFileDialog()} onDrop={(e) => this.drop(e)} />
          <input ref={r => this.inputRef = r} style={{ display: "none" }} type="file" id="input" value="" accept={this.props.accept} multiple={this.props.multiple} onChange={(e) => this.onFileInputChange(e)} />
        </div>
      );
    }

    private openFileDialog() {
      this.inputRef.click();
    }

    private onFileInputChange(formEvent: React.FormEvent<any>): void {
      const files: FileList = formEvent.currentTarget["files"];
      if (!files) {
        return;
      }
      this.enqueueFiles(files);
    }

    private drop(e: React.FormEvent<any>) {
      e.stopPropagation();
      e.preventDefault();

      const dt = e["dataTransfer"];
      if (!dt) {
        console.log("Cannot find dataTransfer on drop!");
        return;
      }

      this.enqueueFiles(dt.files);
    }

    private async enqueueFiles(files: FileList) {
      const fileArray: File[] = [].slice.call(files);
      let am: any[]
      try {
        if (this.props.additionalMetadataProvider) {
          am = await this.props.additionalMetadataProvider(fileArray)
        }
      } catch (e) {
        return
      }

      this.uploader.enqueueFiles(fileArray, am || [], this.props.onDone);
    }
  }
}
