import { IBlobReader } from "./config";
import { FileBlobPromises } from "./fileBlobPromises";

export class BlobReader implements IBlobReader {
  read(blob: Blob, progress?: (percentage: number) => void) {
    return FileBlobPromises.readAsArrayBuffer(blob, progress);
  }
}
