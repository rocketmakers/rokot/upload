import { IChunkSizeProvider } from "./config";

export class ChunkSizeProvider implements IChunkSizeProvider {
  constructor(private chunkSize = Math.pow(2, 20)) { } // Set default chunk size to 1MB to comply with Azure upload limits
  //14 = 16k
  //15 = 32k
  //16 = 64k
  //18 = 256k
  //20 = 1mb
  getChunkSize() {
    return this.chunkSize;
  }
}
