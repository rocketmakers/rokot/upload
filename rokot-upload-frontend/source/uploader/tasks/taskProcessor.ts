import {
  ITask,
  ITaskQueue,
  ITaskResult,
  TaskResultFactory,
  TaskResultStatus,
} from "./task";

/** Indicates the processing status of a Task Queue */
export enum TaskQueueStatus {
  /** An item should never be this status - used to indicate value not set! */
  NotSet,
  // Non Final States
  /** Processing is Pending */
  Pending, // Initial State
  /** The Tasks are processing, next() isn't called until task completes ok  */
  Running,
  /** Temporary failed state - user can re-trigger processing via enqueue on the Processor */
  Error,

  // Final States
  // NOTE: firstFinalStatus = Complete (below)
  /** Complete with no problems */
  Complete,
  /** Fatal failed state - a task failed with a fatal response (including any runtime uncaught errors) */
  Fatal,
  /** Cancelled by the user */
  Cancelled,
}

/** Indicates the First Final State, to simplify logic of preventing update or indicating if processing needs to exit */
const firstFinalStatus = TaskQueueStatus.Complete;

export function isFinalStatus(status: TaskQueueStatus) {
  return status >= firstFinalStatus;
}

function command<T>(execute: (value: T) => void, canExecute: () => boolean) {
  return (value: T) => {
    if (!canExecute()) {
      return;
    }
    execute(value);
  };
}

function clearTimeoutShim(handler: number): boolean {
  clearTimeout(handler);
  return true;
}

function timeout<T>(
  promise: Promise<T>,
  timeout: any,
  errorMessage = "Operation timed out"
) {
  return new Promise<T>(function (fulfill, reject) {
    let hasTimeout = false;
    const handler: any = setTimeout(() => {
      hasTimeout = true;
      reject(new Error(errorMessage));
    }, timeout);
    promise.then(
      command(fulfill, () => !hasTimeout && clearTimeoutShim(handler)),
      command(reject, () => !hasTimeout && clearTimeoutShim(handler))
    );
  });
}

/** Used to auditResult the processing results of the individual task returned from the Task Queue */
export class TaskProcessingResult {
  date = new Date().getTime();
  constructor(
    public taskName: string,
    public status: TaskResultStatus,
    public message?: string
  ) {}
}

/** The maintained Task Queue runtime context - indicating state and provide auditResult services */
class TaskQueueContext {
  /** The runtime Task Queue Status (always initialized as Pending )*/
  private _status = TaskQueueStatus.Pending;

  /** The auditResult trail of the Task Queue processing */
  audits: TaskProcessingResult[] = [];

  constructor(public queue: ITaskQueue) {}

  /** The runtime status */
  get status() {
    return this._status;
  }

  /** Status setter, Preventing update if current state is already final */
  set status(newStatus: TaskQueueStatus) {
    if (isFinalStatus(this._status)) {
      return;
    }

    this._status = newStatus;
  }

  // A fluent method of auditing the Task Result - returns the Task Result for further processing
  auditResult(task: ITask, result: ITaskResult): ITaskResult {
    this.audits.push(
      new TaskProcessingResult(
        task ? task.name : "",
        result.status,
        result.message
      )
    );
    return result;
  }
}

/**
Drains multiple Task Queues respecting a specified maximum concurrency,
Each Task Queue is drained sequentially,
The Task Queue next() method is called initially to get a Task, then again upon completion of the current task
when next() returns null it indicates end of queue/no more tasks
*/
export class SequentialTaskQueueProcessor {
  constructor(
    private notifyChange?: () => void,
    /** Indicate the maximum number of Task Queues this processer will process at a time */
    private maxConcurrency = 3,
    /** Maximum length of time a task can run for before being auto timed out */
    private taskTimeout = 300000
  ) {}

  /** Indicate the current number of Task Queues this processer is processing */
  private currentConcurrency = 0;

  /** internally maintained context for each Task Queue */
  private contexts: TaskQueueContext[] = [];

  /** indicates the Processor is active */
  get isRunning() {
    return this.currentConcurrency > 0;
  }

  /** indicates the number of 'non final state' Task Queues */
  get pendingCount() {
    return this.contexts.filter((q) => !isFinalStatus(q.status)).length;
  }

  /** Enqueue All Task Queues */
  enqueueAll(queues: ITaskQueue[]) {
    queues.forEach((q) => this.enqueue(q));
  }

  /** Enqueue a Task Queue for processing */
  enqueue(queue: ITaskQueue) {
    const context = this.getContext(queue);
    if (context) {
      if (isFinalStatus(context.status)) {
        return;
      }

      if (context.status === TaskQueueStatus.Error) {
        context.status = TaskQueueStatus.Pending;
      }
    } else {
      this.contexts.push(new TaskQueueContext(queue));
    }

    this.processQueue();
  }

  /** Remove a Task Queue from the processor */
  remove(queue: ITaskQueue) {
    this.contexts = this.contexts.filter((context) => {
      if (context.queue !== queue) {
        return true;
      }

      context.status = TaskQueueStatus.Cancelled;
    });
  }

  /** Cancel ths specified Task Queue from processing */
  cancel(queue: ITaskQueue) {
    const context = this.getContext(queue);
    if (!context) {
      return;
    }

    context.status = TaskQueueStatus.Cancelled;
  }

  /** Get the current Task Queue list */
  get queues() {
    return this.contexts.map((c) => c.queue);
  }

  /** Get the Status of the Task Queue */
  getStatus(queue: ITaskQueue) {
    const context = this.getContext(queue);
    return context ? context.status : TaskQueueStatus.NotSet;
  }

  /** Get the Status of the Task Queue */
  getAudits(queue: ITaskQueue): TaskProcessingResult[] {
    const context = this.getContext(queue);
    return context ? context.audits : [];
  }

  private getContext(queue: ITaskQueue) {
    return this.contexts.filter((q) => q.queue === queue)[0];
  }

  private emitChange() {
    if (this.notifyChange) {
      this.notifyChange();
    }
  }

  private processQueue() {
    if (
      !this.contexts.length ||
      this.currentConcurrency >= this.maxConcurrency
    ) {
      return;
    }

    const context = this.contexts.filter(
      (q) => q.status === TaskQueueStatus.Pending
    )[0];
    if (context) {
      context.status = TaskQueueStatus.Running;
      this.currentConcurrency++;
      this.emitChange();
      try {
        this.recurseTaskQueue(context).then(
          (ok) => {
            this.handleItemCompletion(context, ok);
          },
          (err) => {
            this.handleItemCompletion(context, TaskResultFactory.error(err));
          }
        );
      } catch (e) {
        this.handleItemCompletion(context, TaskResultFactory.fatal(e));
      }
    }
  }

  private handleItemCompletion(context: TaskQueueContext, result: ITaskResult) {
    if (result) {
      context.status = this.getQueueStatus(result.status);
    }

    this.currentConcurrency--;
    this.emitChange();
    this.processQueue();
  }

  private getQueueStatus(status: TaskResultStatus): TaskQueueStatus {
    switch (status) {
      case TaskResultStatus.Ok:
        return TaskQueueStatus.Complete;
      case TaskResultStatus.Error:
        return TaskQueueStatus.Error;
      case TaskResultStatus.Fatal:
        return TaskQueueStatus.Fatal;
      default:
        return TaskQueueStatus.Fatal;
    }
  }

  private recurseTaskQueue(context: TaskQueueContext): Promise<ITaskResult> {
    if (isFinalStatus(context.status)) {
      return Promise.resolve(TaskResultFactory.ok());
    }

    let task: ITask;
    try {
      task = context.queue.next();
    } catch (e) {
      let r = TaskResultFactory.fatal(
        "Unable to get next task: " +
          SequentialTaskQueueProcessor.errorString(e)
      );
      return Promise.resolve(context.auditResult(null, r));
    }

    if (task === null) {
      return Promise.resolve(TaskResultFactory.ok());
    }

    try {
      return timeout(task.run(), this.taskTimeout).then(
        (result) => {
          context.auditResult(task, result);
          if (result.status === TaskResultStatus.Ok) {
            this.emitChange();
            return this.recurseTaskQueue(context);
          }

          return result;
        },
        (err) => {
          let r = TaskResultFactory.error(
            SequentialTaskQueueProcessor.errorString(err)
          );
          return context.auditResult(task, r);
        }
      );
    } catch (e) {
      let r = TaskResultFactory.fatal(
        SequentialTaskQueueProcessor.errorString(e)
      );
      return Promise.resolve(context.auditResult(task, r));
    }
  }

  private static errorString(err: any): string {
    if (typeof err === "string") {
      return err;
    }
    return err.toString();
  }
}
