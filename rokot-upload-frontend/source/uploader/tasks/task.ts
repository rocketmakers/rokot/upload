/** Describes a Task */
export interface ITask {
  name: string;
  run(): Promise<ITaskResult>;
}

/** Describes a Queue of Tasks */
export interface ITaskQueue {
  name: string;
  next(): ITask;
}

/** Task Result Status */
export enum TaskResultStatus {
  /** OK */
  Ok,
  /** Error, Allow Retry */
  Error,
  /** Fail, No Retry permitted */
  Fatal,
}

/** Describes the Result of a Task */
export interface ITaskResult {
  /** A message to describe error status or add context to status change */
  message?: string;
  /** The Task Result Status */
  status: TaskResultStatus;
}

/** Produce Task results */
export class TaskResultFactory {
  /** Task processed ok */
  static ok(message?: string): ITaskResult {
    return { status: TaskResultStatus.Ok, message };
  }

  /** Error Task result */
  static error(message: string): ITaskResult {
    return { status: TaskResultStatus.Error, message };
  }

  /** Fatal Task result */
  static fatal(message: string): ITaskResult {
    return { status: TaskResultStatus.Fatal, message };
  }
}

/** Simple implementation of a Task */
export class Task implements ITask {
  constructor(public name: string, public run: () => Promise<ITaskResult>) { }
}
