import { IThumbnailProvider } from "./config";
import { FileBlobPromises } from "./fileBlobPromises";

export class ThumbnailProvider implements IThumbnailProvider {
  getDataUrl(file: File) {
    return FileBlobPromises.readAsDataURL(file)
  }
}
