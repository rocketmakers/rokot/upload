import { IChunkSizeProvider, IChunkInfo, IFileChunk } from "../config";

interface IFileChunkFailureLog {
  chunkInfo: IChunkInfo;
  failCount: number;
}

/**
Tracks the last byte uploaded by issuing chunk data info and recieving ack/nack on successfull transfer
Chunks are intended to be sent sequentially to the server:
Sequence:
var chunk = tracker.getNextChunk();
var ok = [...Upload chunk data];
if (ok)
  tracker.notifyUploaded(chunk); // Must call to increment 'last byte uploaded' and so the return data from getNextChunk!
else // - (Server Nack)]
  tracker.notifyFailed(chunk) // Optional - used to permit error handling UX!
[Repeat getNextChunk till null/ bytesRemaining === 0]
*/
export class FileChunkTracker {
  private _lastByteUploaded: number = -1;
  private failureLog: IFileChunkFailureLog;
  constructor(private file: File, private chunkSizeProvider: IChunkSizeProvider, private notifyChange?: () => void) {
  }

  get lastByteUploaded() {
    return this._lastByteUploaded;
  }

  get fileName() {
    return this.file.name;
  }

  get fileSize() {
    return this.file.size;
  }

  get getFinalByte() {
    return this.fileSize - 1;
  }

  get bytesRemaining() {
    return this.getFinalByte - this.lastByteUploaded;
  }

  get bytesUploaded() {
    return this.lastByteUploaded + 1;
  }

  private getFailureForStart(startAt: number) {
    if (this.failureLog && startAt === this.failureLog.chunkInfo.startAt) {
      return this.failureLog;
    }
  }

  notifyFailed(chunkInfo: IChunkInfo) {
    var failure = this.getFailureForStart(chunkInfo.startAt);
    if (failure) {
      failure.failCount += 1;
    } else {
      this.failureLog = { chunkInfo, failCount: 1 };
    }
    this.informProgress();
  }

  notifyUploaded(fileChunk: IFileChunk) {
    var info = fileChunk.info;
    if (info.endAt > this.getFinalByte) {
      console.log("ERROR: notified chunk upload has last byte after total!!!!")
      return;
    }

    if (this.lastByteUploaded < info.endAt) {
      this._lastByteUploaded = info.endAt;
      this.failureLog = null;
    } else {
      console.log("ERROR: notified about a chunk upload prior to last byte known to have been sent!!!!")
    }

    this.informProgress();
  }

  private informProgress() {
    if (this.notifyChange) {
      this.notifyChange();
    }
  }

  private partNumber = 0;

  private getNextChunkInfo(): IChunkInfo {
    var startAt = this.lastByteUploaded + 1;
    var failure = this.getFailureForStart(startAt);
    var endAt = (startAt + this.chunkSizeProvider.getChunkSize()) - 1;
    var lastByte = this.getFinalByte;
    return new ChunkInfo(startAt, endAt > lastByte ? lastByte : endAt, this.file.size, ++this.partNumber);
  }

  getNextChunk(): IFileChunk {
    if (this.file.size === 0) {
      console.log("Trying to get chunk info for a file of zero size")
      return null;
    }

    if (!this.bytesRemaining) {
      return null;
    }

    // if (this.failureLog && this.failureLog.failCount === 3) {
    //   console.log("Downloaded failing.. we have hit max retry count on the current chunk")
    //   return null;
    // }

    return new FileChunk(this.getNextChunkInfo(), this.file);
  }
}

class ChunkInfo implements IChunkInfo {
  constructor(public startAt: number, public endAt: number, public size: number, public number: number) {
  }

  equals(info: IChunkInfo) {
    return info && this.startAt === info.startAt && this.endAt === info.endAt && this.size === info.size;
  }
}

class FileChunk implements IFileChunk {
  constructor(public info: IChunkInfo, private file: File) {
  }

  getBlob() {
    return this.file.slice(this.info.startAt, this.info.endAt + 1)
  }


  getContentRange() {
    return `bytes ${this.info.startAt}-${this.info.endAt}/${this.info.size}`;
  }
}
