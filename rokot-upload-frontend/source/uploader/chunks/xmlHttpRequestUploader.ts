import {
  IFileChunkUploader,
  IFileChunk,
  IUploaderConfig,
  IUploadMetadata,
  IBlobReader,
  IThumbnailProvider,
  IUploaderTicketProvider,
  IChunkSizeProvider,
} from "../config";

export interface IFileTransporter {
  upload(
    ticket: string,
    bytes: ArrayBuffer | string,
    fileChunk: IFileChunk,
    progress?: (percentage: number) => void
  ): () => Promise<string>;
}

export class UploaderTicketProvider implements IUploaderTicketProvider {
  constructor(
    private url: string,
    private headers?: () => { [name: string]: string }
  ) {}
  async getTicket(metadata: IUploadMetadata) {
    const headers = this.headers ? this.headers() : {};
    headers["Content-Type"] = "application/json";
    const r: RequestInit = {
      method: "POST",
      body: JSON.stringify(metadata),
      headers: headers as any,
    };

    let resp: Response;
    resp = await fetch(this.url, r);
    if (!resp.ok) {
      let message: string;
      try {
        const j = await resp.json();
        message = j.message;
      } catch (error) {
        throw "Unable to parse upload error message!";
      }

      throw message || "Error getting upload ticket!";
    }

    try {
      const j = await resp.json();
      return j.ticket;
    } catch (error) {
      throw "Unable to parse upload ticket!";
    }
  }
}

export class XMLHttpRequestUploaderConfig implements IUploaderConfig {
  chunkUploader: IFileChunkUploader;
  ticketProvider: IUploaderTicketProvider;
  constructor(
    private url: string,
    withCredentials: boolean,
    blobReader: IBlobReader,
    public thumbnailProvider: IThumbnailProvider,
    public chunkSizeProvider: IChunkSizeProvider,
    headers?: () => { [name: string]: string }
  ) {
    this.chunkUploader = new XMLHttpRequestUploader(
      blobReader,
      new XMLHttpRequestTransporter(url, withCredentials)
    );
    this.ticketProvider = new UploaderTicketProvider(url, headers);
  }
}

class XMLHttpRequestTransporter implements IFileTransporter {
  private xhr: XMLHttpRequest;
  constructor(private url: string, private withCredentials: boolean) {}
  upload(
    ticket: string,
    bytes: ArrayBuffer | string,
    fileChunk: IFileChunk,
    progress?: (percentage: number) => void
  ) {
    return () =>
      new Promise<string>((res, rej) => {
        var xmlHttpRequest = (this.xhr = new XMLHttpRequest());
        xmlHttpRequest.open("PUT", this.url, true);
        this.setHeaders(xmlHttpRequest, fileChunk, ticket);

        xmlHttpRequest.upload.onabort = function (e) {
          rej("Upload cancelled");
        };

        if (progress) {
          xmlHttpRequest.upload.onprogress = (evt: ProgressEvent) => {
            if (evt.lengthComputable) {
              progress(Math.round((evt.loaded / evt.total) * 100));
            }
          };

          xmlHttpRequest.upload.onloadstart = (e) => {
            progress(0);
          };
          xmlHttpRequest.upload.onload = (e) => {
            progress(100);
          };
        }
        xmlHttpRequest.onreadystatechange = (evt: ProgressEvent) => {
          if (xmlHttpRequest.readyState < 4) {
            return;
          }

          if (xmlHttpRequest.status === 0) {
            rej("Network Error");
          } else if (xmlHttpRequest.status >= 400) {
            rej("Server Error");
          } else if (xmlHttpRequest.status === 202) {
            res(JSON.parse(xmlHttpRequest.responseText).fileId);
          } else {
            res(null);
          }
        };

        xmlHttpRequest.send(bytes);
      });
  }

  setHeaders(
    xmlHttpRequest: XMLHttpRequest,
    fileChunk: IFileChunk,
    ticket: string
  ) {
    xmlHttpRequest.setRequestHeader("Content-Ticket", ticket);
    xmlHttpRequest.setRequestHeader(
      "Content-Part-Number",
      fileChunk.info.number.toString()
    );
    xmlHttpRequest.setRequestHeader("Content-Type", "application/octet-stream");
    xmlHttpRequest.setRequestHeader(
      "Content-Range",
      fileChunk.getContentRange()
    );
    if (this.withCredentials) {
      xmlHttpRequest.withCredentials = true;
    }
  }
}

class XMLHttpRequestUploader implements IFileChunkUploader {
  constructor(
    private blober: IBlobReader,
    private transporter: IFileTransporter
  ) {}
  upload(ticket: string, fileChunk: IFileChunk) {
    return new Promise<string>((res, rej) => {
      var blob = fileChunk.getBlob();
      this.uploadChunk(ticket, fileChunk, res, rej);
    });
  }

  private retry(
    promise: () => Promise<string>,
    maxCount = 3,
    count = 1
  ): Promise<string> {
    return promise().catch((err) => {
      if (count === maxCount) {
        throw err;
      }
      return this.retry(promise, maxCount, ++count);
    });
  }

  private uploadChunk(
    ticket: string,
    fileChunk: IFileChunk,
    ok: (value?: string) => void,
    fail: (error?: any) => void
  ) {
    var blob = fileChunk.getBlob();
    this.blober
      .read(blob)
      .then((arr) => {
        this.retry(this.transporter.upload(ticket, arr, fileChunk, (pct) => {}))
          .then((v) => ok(v))
          .catch((e) => fail(e));
      })
      .catch((e) => fail(e));
  }
}
