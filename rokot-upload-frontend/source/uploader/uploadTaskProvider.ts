import { ITask, ITaskQueue, TaskResultFactory } from "./tasks/task";
import { SequentialTaskQueueProcessor, TaskQueueStatus, TaskProcessingResult } from "./tasks/taskProcessor";
import { IFileChunk, IUploadMetadata, IUploaderConfig } from "./config";
import { FileChunkTracker } from "./chunks/fileChunk";

export interface IFileUploadManager {
  fileName: string;
  ticket: string;
  thumbnailUrl: string;
  bytesRemaining: number;
  bytesUploaded: number;
  bytesTotal: number;
  message: string;
  getStatus(): TaskQueueStatus;
  getAudits(): TaskProcessingResult[];
  retry(): void;
  cancel(): void;
  remove(): void;
}

enum UploadItemState {
  Pending,
  Initialized,
  ChunksComplete,
  Finalized
}

class FileUploadManager implements IFileUploadManager {
  constructor(private tracker: FileChunkTracker, private taskProvider: UploadTaskProvider, private processer: SequentialTaskQueueProcessor, private notifyChange: () => void) {
  }

  ticket: string;
  thumbnailUrl: string;
  get message(): string {
    const audits = this.getAudits()
    return audits[audits.length - 1].message
  }

  get fileName() {
    return this.tracker.fileName;
  }

  get bytesTotal(): number {
    return this.tracker.fileSize;
  }

  get bytesRemaining(): number {
    return this.tracker.bytesRemaining;
  }

  get bytesUploaded(): number {
    return this.tracker.bytesUploaded;
  }

  getStatus() {
    return this.processer.getStatus(this.taskProvider);
  }

  getAudits() {
    return this.processer.getAudits(this.taskProvider);
  }

  remove() {
    this.processer.remove(this.taskProvider);
    this.notifyChange();
  }

  retry() {
    this.processer.enqueue(this.taskProvider);
    this.notifyChange();
  }

  cancel() {
    this.processer.cancel(this.taskProvider);
    this.notifyChange();
  }
}

export class UploadTaskProvider implements ITaskQueue {
  private tracker: FileChunkTracker;
  name: string;
  upload: IFileUploadManager;
  constructor(
    private file: File,
    private additionalMetadata: any,
    private config: IUploaderConfig,
    processer: SequentialTaskQueueProcessor,
    private notifyChange: () => void,
    private customFinalizer?: (upload: IFileUploadManager) => Promise<void>
  ) {
    this.name = file.name;
    this.tracker = new FileChunkTracker(file, config.chunkSizeProvider, notifyChange)
    this.upload = new FileUploadManager(this.tracker, this, processer, notifyChange);
    if (file.size < 40000000) {
      // Don't generate thumbnails for anything over 40mb. This lags out the browser completely
      var objectURL = window.URL.createObjectURL(file);
      this.upload.thumbnailUrl = objectURL;
    }
  }

  private state = UploadItemState.Pending
  private uploadedFilename: string;

  next(): ITask {
    switch (this.state) {
      case UploadItemState.Pending:
        return this.getTicketTask();
      case UploadItemState.Initialized:
        return this.getChunkUploadTask();
      case UploadItemState.ChunksComplete:
        return this.getFinalizerTask();
      case UploadItemState.Finalized:
        this.notifyChange()
        return null;
    }
  }

  private transitionState(state: UploadItemState) {
    this.state = state
    this.notifyChange()
  }

  private getChunkUploadTask() {
    const chunk = this.tracker.getNextChunk();
    if (chunk) {
      return this.sendChunkTask(chunk);
    }
    this.transitionState(UploadItemState.ChunksComplete);
    return this.chunksCompleteTask();
  }

  private getFinalizerTask() {
    const promise: Promise<void> = this.customFinalizer ? this.customFinalizer(this.upload) : Promise.resolve<void>(undefined)
    return {
      name: "Finalizer",
      run: () => {
        return promise.then(() => {
          this.transitionState(UploadItemState.Finalized)
          return TaskResultFactory.ok();
        }).catch(err => {
          console.error(err);
          this.notifyChange();
          return TaskResultFactory.error(`error finalizing: ${err}`);
        })
      }
    };
  }

  private getTicketTask() {
    return {
      name: "Get ticket",
      run: () => this.config.ticketProvider.getTicket(this.getUploadMetadata(this.file, this.additionalMetadata)).then(t => {
        if (!t) {
          return TaskResultFactory.error("Unable to get upload ticket");
        }
        this.upload.ticket = t;
        this.transitionState(UploadItemState.Initialized)
        return TaskResultFactory.ok();
      })
    };
  }

  private getUploadMetadata(file: File, additionalMetadata: any): IUploadMetadata {
    const overrideFileName = additionalMetadata.overrideFileName
    let name = file.name
    if (overrideFileName) {
      additionalMetadata.originalFileName = name
      delete additionalMetadata.overrideFileName
      name = overrideFileName
    }

    const overrideContentType = additionalMetadata.overrideContentType
    let contentType = file.type
    if (overrideContentType) {
      contentType = overrideContentType
      delete additionalMetadata.overrideContentType
    }

    return { name, size: file.size, contentType, additionalMetadata };
  }

  private sendChunkTask(chunk: IFileChunk) {
    return {
      name: "Send Chunk",
      run: () => this.sendChunk(chunk)
    };
  }

  private chunksCompleteTask() {
    return {
      name: "Chunks Complete",
      run: () => Promise.resolve(TaskResultFactory.ok())
    };
  }

  private sendChunk(chunk: IFileChunk) {
    return this.config.chunkUploader.upload(this.upload.ticket, chunk).then(fileName => {
      if (!fileName) {
        this.tracker.notifyUploaded(chunk);
        this.notifyChange();
        return TaskResultFactory.ok()
      } else {
        this.tracker.notifyUploaded(chunk);
        this.uploadedFilename = fileName;
        this.notifyChange();
        return TaskResultFactory.ok()
      }
    }, err => {
      this.tracker.notifyFailed(chunk.info);
      this.notifyChange()
      return TaskResultFactory.error("Upload chunk failed: " + err.toString());
    })
  }
}
