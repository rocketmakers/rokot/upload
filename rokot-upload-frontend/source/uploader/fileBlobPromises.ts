export class FileBlobPromises {
  static readAsDataURL(
    blob: Blob,
    progress?: (percentage: number) => void
  ): Promise<string> {
    return new Promise<string>((ok, fail) => {
      const reader = FileBlobPromises.createFileReader(fail, progress);
      reader.onloadend = () => {
        ok(reader.result as any);
      };

      reader.readAsDataURL(blob);
    });
  }

  static readAsArrayBuffer(
    blob: Blob,
    progress?: (percentage: number) => void
  ): Promise<ArrayBuffer | string> {
    return new Promise<ArrayBuffer | string>((ok, fail) => {
      const reader = FileBlobPromises.createFileReader(fail, progress);
      reader.onloadend = function (evt) {
        if (evt.target.readyState === 2) {
          // DONE == 2
          ok(evt.target.result);
        }
      };

      reader.readAsArrayBuffer(blob);
    });
  }

  private static createFileReader(
    fail: (msg: string) => void,
    progress?: (percentage: number) => void
  ) {
    const reader = new FileReader();
    reader.onerror = (e) => {
      const msg = this.errorMessage(e);
      if (msg) {
        fail(msg);
      }
    };
    reader.onabort = function (e) {
      fail("File read cancelled");
    };
    if (progress) {
      reader.onprogress = (evt: ProgressEvent) => {
        if (evt.lengthComputable) {
          progress(Math.round((evt.loaded / evt.total) * 100));
        }
      };
      reader.onloadstart = (e) => {
        progress(0);
      };
      reader.onload = (e) => {
        progress(100);
      };
    }

    return reader;
  }

  private static errorMessage(evt) {
    const err = evt.target.error;
    switch (err.code) {
      case err.NOT_FOUND_ERR:
        return "Unable to Read - File Not Found!";
      case err.NOT_READABLE_ERR:
        return "File is not readable";
      case err.ABORT_ERR:
        return null;
      default:
        return "An error occurred reading this file.";
    }
  }
}
