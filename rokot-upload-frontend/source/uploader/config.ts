export interface IChunkInfo {
  startAt: number;
  endAt: number;
  size: number;
  number: number;
}

export interface IFileChunk {
  info: IChunkInfo;
  getBlob(): Blob;
  getContentRange(): string;
}

export interface IFileChunkUploader {
  upload(ticket: string, fileChunk: IFileChunk): Promise<string>;
}

export interface IUploadMetadata {
  name: string;
  size: number;
  contentType: string;
  additionalMetadata: any;
}

export interface IUploaderConfig {
  chunkUploader: IFileChunkUploader;
  chunkSizeProvider: IChunkSizeProvider;
  ticketProvider: IUploaderTicketProvider;
  thumbnailProvider: IThumbnailProvider;
}

export interface IChunkSizeProvider {
  getChunkSize(): number;
}

export interface IUploaderTicketProvider {
  getTicket(metadata: IUploadMetadata): Promise<string>;
}

export interface IThumbnailProvider {
  getDataUrl(file: File): Promise<string>;
}

export interface IBlobReader {
  read(
    blob: Blob,
    progress?: (percentage: number) => void
  ): Promise<ArrayBuffer | string>;
}
