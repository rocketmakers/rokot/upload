export interface IUploadLogger {
  //getById(id: string, context: IDataContext): Promise<IUpload | null>
  log(upload: IUpload): Promise<void>
  logComplete(ticket: string): Promise<void>
}
export interface IUpload {
  id?: number,
  ticket: string
  fileName: string
  fileSize: number
  contentType: string
  userToken?: any
  additionalMetadata?: any
  createdOn?: string
}

export interface IJwtConfig {
  key: string
}
