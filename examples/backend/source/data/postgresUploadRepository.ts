import { KnexConnection } from "./knexConnection";
import { IUploadLogger, IUpload } from "../core";

interface IDbUpload {
  id?: number,
  ticket: string
  file_name: string
  file_size: number
  content_type: string
  user_token?: any
  additional_metadata?: any
  created_on: string
}

function fromDb(dbUpload: IDbUpload): IUpload {
  return {
    id: dbUpload.id,
    ticket: dbUpload.ticket,
    fileName: dbUpload.file_name,
    fileSize: dbUpload.file_size,
    contentType: dbUpload.content_type,
    userToken: dbUpload.user_token,
    additionalMetadata: dbUpload.additional_metadata,
    createdOn: dbUpload.created_on
  };
}

function toDb(upload: IUpload): IDbUpload {
  return {
    //id: upload.id,
    ticket: upload.ticket,
    file_name: upload.fileName,
    file_size: upload.fileSize,
    content_type: upload.contentType,
    additional_metadata: upload.additionalMetadata,
    user_token: JSON.stringify(upload.userToken),
    created_on: new Date().toISOString()
  }
}

export class PostgresUploadRepository implements IUploadLogger {
  constructor(private connection: KnexConnection) { }

  async getById(id: string): Promise<IUpload | null> {
    const rows: any[] = await this.connection.knex("upload").where({ id })
    if (!rows.length) {
      return null;
    }

    return fromDb(rows[0]);
  }

  async log(upload: IUpload): Promise<void> {
    const result = await this.connection.knex("upload")
      .insert(toDb(upload)).returning("id")
    return result[0];
  }

  async logComplete(ticket: string): Promise<void> {
    const result = await this.connection.knex("upload")
      .update({ complete: true }).where({ ticket })
  }
}
