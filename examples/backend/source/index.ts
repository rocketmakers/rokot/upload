import { run } from "./ioc/bootstrap";
import { ConsoleLogger } from "rokot-log";
import { getFileStoreConfig } from "./fileStoreConfig";

const logger = ConsoleLogger.create("Rokot Upload Example API", { level: "trace" })

run(
  {
    client: "pg",
    connection: {
      host: 'localhost', //"postgres"
      user: "postgres",
      password: "rocketmakers",
      database: "rokot-upload-example"
    },
    searchPath: "api"
  },
  { port: 3000 },
  getFileStoreConfig(),
  { key: "" },
  logger).then(v => {
    console.log(`Run exited with: ${v}`)
  })
