import * as path from "path";
import { IS3FileStoreConfig, ILocalFileStoreConfig, IFileStoreConfig, IAzureFileStoreConfig, IGCSFileStoreConfig } from "../../../rokot-upload-backend";

function getEnvKey(key: string) {
  return process.env[key] as string
}

function makeGCSConfig(): IGCSFileStoreConfig {
  const saPath = getEnvKey("GOOGLE_APPLICATION_CREDENTIALS")
  if (!saPath) {
    return;
  }
  return {
    gcs: { projectId: getEnvKey("PROJECT_ID") || "test" },
    bucketName: getEnvKey("BUCKET_NAME") || "test"
  }
}

function makeAzureConfig(): IAzureFileStoreConfig {
  const con = getEnvKey("AZURE_CONNECTION_STRING")
  if (!con) {
    return;
  }
  return {
    azure: { connectionString: con },
    containerName: "test"
  }
}

function makeS3Config(): IS3FileStoreConfig {
  const region = getEnvKey("AWS_REGION")
  if (!region) {
    return;
  }
  return {
    aws:
    {
      region,
      accessKeyId: getEnvKey("ACCESS_KEY_ID"),
      secretAccessKey: getEnvKey("SECRET_ACCESS_KEY"),
    },
    bucketName: getEnvKey("BUCKET_NAME") || "rokot-upload-example"
  }
}

function makeLocalConfig(): ILocalFileStoreConfig {
  return {
    pathProvider: (ticket, extension) => {
      const parts = ticket.split("|")
      return path.resolve(`./uploads_cache/${parts[0]}.${extension}`)
    },
    createTicket: (filename, metadata) => `ticket_${Date.now()}_${parseInt((Math.random() * 100000) as any)}`
  }
}

export function getFileStoreConfig(): IFileStoreConfig {
  return makeGCSConfig() || makeAzureConfig() || makeS3Config() || makeLocalConfig()
}
