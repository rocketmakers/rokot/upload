export class InjectTypes {
  static KnexConfig = "KnexConfig"
  static KnexConnection = "KnexConnection"
  static PassportAuth = "PassportAuth"

  static Logger = "Logger"

  static JwtConfig = "JwtConfig"
  static ApiConfig = "ApiConfig"
  static Api = "Api"

  static UploadController = "UploadController"
  static JwtController = "JwtController"
  static UploadRepository = "UploadRepository"

  static FileStore = "FileStore"
  static FileStoreConfig = "FileStoreConfig"

  static CreateFileService = "CreateFileService"
  static PutChunkService = "PutChunkService"
  static FilenameGenerator = "FilenameGenerator"

  static ContentRangeParser = "ContentRangeParser"
}
