import { kernel, register } from "./kernel";
import { InjectTypes } from "./injectTypes";
import { UploadController } from "../controllers/uploadController";
import { JwtController } from "../controllers/jwtController";
import { KnexConnection } from "../data/knexConnection";
import { PostgresUploadRepository } from "../data/postgresUploadRepository";
import { Logger } from "rokot-log";
import { IApiConfig, Api } from "../server/api";
import { PassportAuth } from "../server/passport";
import { IJwtConfig } from "../core";
import * as knexImport from "knex";

import {
  //IFilenameGenerator,
  UuidFilenameGenerator,
  //IPutChunkService,
  PutChunkService,
  //ICreateFileService,
  CreateFileService,
  ContentRangeParser,
  LocalFileStore,
  ILocalFileStoreConfig,
  isLocalFileStoreConfig,
  S3FileStore,
  isS3FileStoreConfig,
  IS3FileStoreConfig,
  IFileStoreConfig,
  isAzureFileStoreConfig,
  IAzureFileStoreConfig,
  AzureFileStore,
  isGCSFileStoreConfig,
  IGCSFileStoreConfig,
  GCSFileStore
} from "../../../../rokot-upload-backend";

register(InjectTypes.KnexConnection, KnexConnection,
  InjectTypes.Logger,
  InjectTypes.KnexConfig)

register(InjectTypes.PassportAuth, PassportAuth, InjectTypes.Logger, InjectTypes.JwtConfig).inSingletonScope()
register(InjectTypes.ContentRangeParser, ContentRangeParser).inSingletonScope()
register(InjectTypes.FilenameGenerator, UuidFilenameGenerator).inSingletonScope()
register(InjectTypes.CreateFileService, CreateFileService, InjectTypes.FileStore, InjectTypes.FilenameGenerator).inSingletonScope()
register(InjectTypes.PutChunkService, PutChunkService, InjectTypes.FileStore, InjectTypes.ContentRangeParser).inSingletonScope()

register(InjectTypes.Api, Api, InjectTypes.Logger, InjectTypes.ApiConfig, InjectTypes.PassportAuth)
register(InjectTypes.UploadRepository, PostgresUploadRepository, InjectTypes.KnexConnection)
register(InjectTypes.UploadController, UploadController, InjectTypes.UploadRepository, InjectTypes.CreateFileService, InjectTypes.PutChunkService)
register(InjectTypes.JwtController, JwtController, InjectTypes.JwtConfig)


function bindFileStore(fileStoreConfig: IFileStoreConfig, logger: Logger) {
  if (isGCSFileStoreConfig(fileStoreConfig)) {
    logger.info("-- -- GCS File System")
    kernel.bind<IGCSFileStoreConfig>(InjectTypes.FileStoreConfig).toConstantValue(fileStoreConfig)
    register(InjectTypes.FileStore, GCSFileStore, InjectTypes.FileStoreConfig).inSingletonScope()
    return
  }

  if (isAzureFileStoreConfig(fileStoreConfig)) {
    logger.info("-- -- Azure File System")
    kernel.bind<IAzureFileStoreConfig>(InjectTypes.FileStoreConfig).toConstantValue(fileStoreConfig)
    register(InjectTypes.FileStore, AzureFileStore, InjectTypes.FileStoreConfig).inSingletonScope()
    return
  }

  if (isS3FileStoreConfig(fileStoreConfig)) {
    logger.info("-- -- S3 File System")
    kernel.bind<IS3FileStoreConfig>(InjectTypes.FileStoreConfig).toConstantValue(fileStoreConfig)
    register(InjectTypes.FileStore, S3FileStore, InjectTypes.FileStoreConfig).inSingletonScope()
    return
  }

  if (isLocalFileStoreConfig(fileStoreConfig)) {
    logger.info("-- -- Local File System")
    kernel.bind<ILocalFileStoreConfig>(InjectTypes.FileStoreConfig).toConstantValue(fileStoreConfig)
    register(InjectTypes.FileStore, LocalFileStore, InjectTypes.FileStoreConfig).inSingletonScope()
  }
}

export async function run(knexConfig: knexImport.Config, apiConfig: IApiConfig, fileStoreConfig: IFileStoreConfig, jwtConfig: IJwtConfig, logger: Logger) {
  logger.info("Binding")
  logger.info("-- Core Services")

  kernel.bind<Logger>(InjectTypes.Logger).toConstantValue(logger)

  logger.info("-- Config")
  kernel.bind<IApiConfig>(InjectTypes.ApiConfig).toConstantValue(apiConfig)
  kernel.bind<IJwtConfig>(InjectTypes.JwtConfig).toConstantValue(jwtConfig)
  bindFileStore(fileStoreConfig, logger)
  kernel.bind<knexImport.Config>(InjectTypes.KnexConfig).toConstantValue(knexConfig)

  logger.info("Testing Connection")
  const connection = kernel.get<KnexConnection>(InjectTypes.KnexConnection);
  const ok = await connection.test()
  if (!ok) {
    logger.info("-- Connection Failed")
    connection.destroy()
    return false
  }
  logger.info("-- Passed")

  logger.info("Starting API")
  const api = kernel.get<Api>(InjectTypes.Api)
  try {
    if (!api.run()) {
      logger.info("-- Failed")
      return false
    }
    logger.info("-- Started")
    return true;
  } catch (e) {
    logger.error(e, "-- Failed")
  }
}
