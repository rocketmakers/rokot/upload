import { Container, decorate, injectable, inject, interfaces } from "inversify";
export const kernel = new Container();

export function register<T>(key: string, classType: { new(...args): T }, ...ctorParams: string[]) {
  decorate(injectable(), classType);
  ctorParams.forEach((c, i) => {
    decorate(inject(c), classType, i);
  })
  return kernel.bind(key).to(classType)
}
