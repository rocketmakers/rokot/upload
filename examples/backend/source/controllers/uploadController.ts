import { api } from "rokot-apicontroller";
import * as bodyParser from "body-parser";
import { IRequest } from "../server/expressRequest";
import { InjectTypes } from "../ioc/injectTypes";
import {
  ICreateFileService,
  IPutChunkService,
  IUploadMetadata,
  IFileCreationResult
} from "../../../../rokot-upload-backend";
import { IUploadLogger, IUpload } from "../core";

function fromMetadata(uploadMetadata: IUploadMetadata, ticket: string, userToken?: any): IUpload {
  return {
    ticket,
    fileName: uploadMetadata.name,
    fileSize: uploadMetadata.size,
    contentType: uploadMetadata.contentType,
    additionalMetadata: uploadMetadata.additionalMetadata,
    userToken
  };
}

class Middleware {
  @api.middlewareFunction("json")
  static json = bodyParser.json()

  @api.middlewareFunction("raw")
  static raw = bodyParser.raw({ limit: '10mb' })
}

@api.controller(InjectTypes.UploadController)
export class UploadController {
  constructor(
    private uploadLogger: IUploadLogger,
    private createFileService: ICreateFileService,
    private putChunkService: IPutChunkService
  ) { }

  @api.route("upload")
  @api.verbs("post")
  @api.middleware("json")
  @api.middleware("protect")
  @api.middleware("authenticate")
  async createFile(req: IRequest<IUploadMetadata, IFileCreationResult, void, void>) {
    const result = await this.createFileService.createFile(req.body);
    await this.uploadLogger.log(fromMetadata(req.body, result.filename, req.native.request.user))
    req.sendOk(result);
  }

  @api.route("upload")
  @api.verbs("put")
  @api.middleware("raw")
  async putChunk(req: IRequest<Buffer, void, void, void>) {
    const ticket = req.headers["content-ticket"]
    const partNumber = parseInt(req.headers["content-part-number"]);
    const rangeHeader = req.headers["content-range"];
    const r = await this.putChunkService.putChunk(ticket, req.body, partNumber, rangeHeader);
    if (r.complete) {
      await this.uploadLogger.logComplete(r.filename)
    }
    req.sendNoContent();
  }
}
