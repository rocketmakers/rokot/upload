import { api } from "rokot-apicontroller";
import * as jwt from "jsonwebtoken";
import { InjectTypes } from "../ioc/injectTypes";
import { IJwtConfig } from "../core";
import { IRequest } from "../server/expressRequest";

@api.controller(InjectTypes.JwtController, "jwt")
export class JwtController {
  constructor(private config: IJwtConfig) { }
  @api.route("login")
  @api.verbs("post")
  async loginJwt(req: IRequest<void, { token: string }, void, void>) {
    const token = jwt.sign({ user: "guest" }, this.config.key);
    req.sendOk({ token });
  }
}