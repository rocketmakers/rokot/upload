import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as passport from 'passport';
import { Logger } from "rokot-log";

import { api } from "rokot-apicontroller";

import { Strategy as JwtStrategy, ExtractJwt } from "passport-jwt";
import { IJwtConfig } from "../core";

class Middleware {
  static jwtConfig: IJwtConfig
  @api.middlewareFunction("authenticate")
  static jwtAuthenticate(req: express.Request, res: express.Response, next: express.NextFunction) {
    if (!Middleware.jwtConfig.key) {
      return next()
    }
    passport.authenticate("jwt", { session: false }, (err: Error, user: any, info: any) => {
      if (err) {
        return next(err);
      }
      if (!user) {
        return next();
      }
      if (user) {
        req.login(user, { session: false }, err => {
          if (err) {
            return next(err);
          }
          return next();
        })
      }
    })(req, res, next);
  }

  @api.middlewareFunction("protect")
  static protect(req: express.Request, res: express.Response, next: express.NextFunction) {
    if (Middleware.jwtConfig.key && !req.isAuthenticated()) {
      res.status(401).send("Unauthenticated");
      return
    }
    next()
  }
}

export class PassportAuth {
  constructor(private logger: Logger, private jwtConfig: IJwtConfig) {
    Middleware.jwtConfig = jwtConfig
  }

  private applyJwt(secretOrKey: string) {
    if (!secretOrKey) {
      this.logger.warn("-- Jwt auth not applied as no key provided!")
      return
    }
    this.logger.info("-- Jwt")
    passport.use(new JwtStrategy({
      secretOrKey,
      jwtFromRequest: ExtractJwt.fromAuthHeader()
    }, async (payload, done) => {
      this.logger.info("-- Jwt Payload", payload)
      if (!payload) {
        return done(null, false);
      }
      done(null, payload);
    }))
  }


  setup(app: express.Express) {
    this.logger.info("Passport")
    this.applyJwt(this.jwtConfig.key)

    passport.serializeUser((user, done) => {
      done(null, user);
    });

    passport.deserializeUser(async (user, done) => {
      try {
        done(null, user);
      } catch (err) {
        done(err, null);
      }
    })

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));

    app.use(passport.initialize());
  }
}
