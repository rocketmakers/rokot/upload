CREATE TABLE "upload" (
  id serial PRIMARY KEY,
  ticket text NOT NULL,
  file_name text NOT NULL,
  file_size int NOT NULL,
  content_type text NOT NULL,
  user_token JSONB,
  additional_metadata JSONB,
  created_on timestamp NOT NULL,
  complete boolean NULL
);

CREATE UNIQUE INDEX upload_unique_ticket ON "upload" (ticket);
CREATE INDEX upload_token_gin ON "upload" USING gin (user_token);
CREATE INDEX upload_metadat_gin ON "upload" USING gin (additional_metadata);
