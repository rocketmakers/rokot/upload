import * as React from "react";
import {
  withFileUpload,
  IFileUploadComponentProps,
  TaskQueueStatus,
  TaskProcessingResult,
  TaskQueueStatusHelper,
  IFileUploadManager
} from "../../../../../rokot-upload-frontend";
import { Grid, Row, Col, Button, Icon } from "armstrong-react";

import "./exampleFileUpload.scss";

function getTaskQueueStatus(status: TaskQueueStatus) {
  switch (status) {
    case TaskQueueStatus.Pending:
      return "Pending"
    case TaskQueueStatus.Running:
      return "Running"
    case TaskQueueStatus.Error:
      return "Error"
    case TaskQueueStatus.Complete:
      return "Complete"
    case TaskQueueStatus.Cancelled:
      return "Cancelled"
    case TaskQueueStatus.Fatal:
      return "Fatal"
  }

  return "[Unknown]"
}

interface ExampleFileUploadComponentProps {
  debug?: boolean;
}

class ExampleFileUploadComponent extends React.Component<ExampleFileUploadComponentProps & IFileUploadComponentProps, {}> {
  render() {
    return (
      <div>
        {this.props.uploads.map((u, i) => <Upload key={u.fileName} debug={this.props.debug} manager={u} />)}
        <div className="drop-area" onDrop={e => this.props.onDrop(e)} onDragOver={(e) => e.preventDefault()}>
          <div><Icon icon={Icon.Icomoon.image} />Drop image(s) here to upload</div>
        </div>
        <Button onClick={() => this.props.openFileDialog()}>Upload</Button>
      </div>
    );
  }
}

class Upload extends React.Component<{ manager: IFileUploadManager, debug: boolean }, {}> {
  render() {
    if (this.props.debug) {
      return <DebugUpload manager={this.props.manager} />
    }

    return <SimpleUpload manager={this.props.manager} />
  }
}

class SimpleUpload extends React.Component<{ manager: IFileUploadManager }, {}> {
  render() {
    const status = this.props.manager.getStatus();
    var progress = Math.round((this.props.manager.bytesUploaded / this.props.manager.bytesTotal) * 100);
    return (<Grid className="upload-item form-grid">
      <Row>
        <Col width={150} className="m-right-xsmall">
          <div className="upload-thumb" style={{ backgroundImage: `url(${this.props.manager.thumbnailUrl})` }}></div>
        </Col>
        <Col>

          <Grid className="status-header form-grid">
            <Row>
              <Col verticalAlignment="center" horizontalAlignment="left">
                {getTaskQueueStatus(status)}
              </Col>
              <Col verticalAlignment="center" horizontalAlignment="right">
                {!TaskQueueStatusHelper.isFinal(status) &&
                  <Button className="bg-negative p-xsmall" leftIcon={Icon.Icomoon.cross2} onClick={() => this.props.manager.cancel()}>Cancel</Button>
                }
                {TaskQueueStatusHelper.canRetry(status) &&
                  <Button className="bg-info p-xsmall" leftIcon={Icon.Icomoon.loop3} onClick={() => this.props.manager.retry()}>Retry</Button>
                }
              </Col>
            </Row>
          </Grid>


          <div className="progress-wrapper">
            <div className="progress" style={{ width: `${progress}%` }}>
            </div>
          </div>
          <div>
            <div className="clearfix">
              <div className="status-text">{this.props.manager.fileName}</div>
              <div className="percentage-text">{`${progress}%`}</div>
            </div>
          </div>
        </Col>
      </Row>
    </Grid>)
  }
}

class DebugUpload extends React.Component<{ manager: IFileUploadManager }, {}> {
  render() {
    const { manager } = this.props;
    const status = manager.getStatus();

    return (
      <Grid>
        <Row>
          <Col>
            {manager.thumbnailUrl && <img src={manager.thumbnailUrl} width={128} />}
          </Col>
          <Col>
            <span>Status: </span>
            <span>{getTaskQueueStatus(status)}</span>
          </Col>
          <Col>
            <span>Filename: </span>
            <span>{manager.fileName}</span>
          </Col>
        </Row>
        <Row>
          <Col>
            <span>Total: </span>
            <span>{manager.bytesTotal}</span>
          </Col>
          <Col>
            <span>Uploaded: </span>
            <span>{manager.bytesUploaded}</span>
          </Col>
          <Col>
            <span>Remaining: </span>
            <span>{manager.bytesRemaining}</span>
          </Col>
        </Row>
        <Row>
          <Col>
            {manager.message && <span>Error: {manager.message}</span>}
          </Col>
        </Row>
        <Row>
          <Col>
            {TaskQueueStatusHelper.canRetry(status) && <button onClick={() => manager.retry()}>Retry</button>}
            {!TaskQueueStatusHelper.isFinal(status) && <button onClick={() => manager.cancel()}>Cancel</button>}
            <button onClick={() => manager.remove()}>Remove</button>
          </Col>
        </Row>
        <Row>
          <Col>
            {manager.getAudits().map((u, i) => this.getAuditItem(u, i))}
          </Col>
        </Row>
      </Grid>
    )
  }

  private getAuditItem(result: TaskProcessingResult, key: number) {
    return (
      <div key={key}>
        <span>{result.date}</span>
        <span> -- {result.taskName}</span>
        <span> -- {result.status}</span>
        <span> -- {result.message}</span>
      </div>
    );
  }
}

// function getJwtAuthBearerHeader() {
//   return {"Authorization": "JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiZ3Vlc3QiLCJpYXQiOjE0NzY3NTE4Mzl9.isVsy5kyWIVd9sXk6OnMyD5Yt8LqQFZbPzZcYJVIicU"}
// }
export const ExampleFileUpload = withFileUpload<ExampleFileUploadComponentProps>("http://localhost:3000/upload", false, undefined, Math.pow(2, 20) * 2) // , getJwtAuthBearerHeader
  (ExampleFileUploadComponent);
