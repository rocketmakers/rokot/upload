import * as React from "react";
import { Route, IndexRoute } from "react-router";

// VIEWS
import { Home } from "./views/home";
import { NotFound } from "./views/system/notFound";

export function routeFactory(app: React.ComponentClass<any>): JSX.Element {
    return (
        <Route path="/" component={app}>
            <IndexRoute component={Home} />
            <Route path="*" component={NotFound} />
        </Route>
    )
}
