import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Router, hashHistory } from 'react-router';
import { routeFactory } from "./routes";

// STYLES
import "./theme/theme.scss";

// APP WRAPPER
class App extends React.Component<any, {}> {
  public render() {
    const view = this.props.children;
    return (
      <main>
        {this.props.children}
      </main>
    );
  }
}

ReactDOM.render(
  <Router history={hashHistory}>
    {routeFactory(App)}
  </Router>
  , document.getElementById('host'));
