import * as React from "react";
import { withRouter } from 'react-router';
import { Grid, Row, Col } from "armstrong-react";

import { ExampleFileUpload } from "../components/exampleFileUpload";
import { WithRouterProps } from "react-router/lib/withRouter";

interface IHomeComponentProps extends WithRouterProps {
  //getUsers: () => void;
  //users: IStateFragment<IUser[]>;
}

async function getMetadata(files: File[]) {
  return files.map(f => ({ test: "test" }))
}
export class HomeComponent extends React.Component<IHomeComponentProps, {}> {
  render() {
    return (
      <Grid fillContainer={true}>
        <Row height="auto">
          <Col className="p-medium bg-brand-primary fg-white" horizontalAlignment="center" verticalAlignment="center">
            Welcome to rokot-upload-frontend-example
          </Col>
        </Row>
        <Row>
          <Col className="p-medium">
            <h2>Example uploader</h2>
            <ExampleFileUpload accept="image/*" additionalMetadataProvider={getMetadata} />
          </Col>
        </Row>
      </Grid>
    )
  }
}

export const Home = withRouter(HomeComponent)
