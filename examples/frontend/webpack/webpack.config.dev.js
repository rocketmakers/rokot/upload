/*********************************************
    WEBPACK - DEVELOPMENT COMPILER CONFIG
**********************************************/

var webpack = require('webpack');
var path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");

var APP_DIR = path.join(__dirname, '../source/app');
var buildPath = path.join(__dirname, '../www');


module.exports = {
  entry: { app: path.join(APP_DIR, "index.tsx") },
  devtool: "eval",
  output: {
    filename: "[name].js",
    path: buildPath
  },
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".scss"] // search for files ending with these extensions when importing
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: "ts-loader" // compile typescript
      },
      {
        test: /\.(jpg|ttf|svg|woff|png|mp4|mp3)$/, // add extensions for new file types here
        use: {
          loader: "file-loader", // copy files to proxy and update paths to be absolute
          options: {
            name: "[path][name].[ext]",
            publicPath: "/",
            emitFile: false
          }
        }
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          use: [
            "css-loader", // turn url() and @import calls into require
            {
              loader: "autoprefixer-loader",
              options: {
                browsers: "last 2 version"
              }
            }, // adds prefixes to support older browsers
            "sass-loader" // compile sass
          ]
        })
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin({ filename: "[name].css" }), 
    new CopyWebpackPlugin([{ from: path.resolve('./dev.html'), to: 'index.html' }]),
    new CopyWebpackPlugin([{ from: "source/app/assets", to: "source/app/assets" }])]
}